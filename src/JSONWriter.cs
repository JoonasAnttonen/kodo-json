﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Kodo.JSON
{
    /// <summary> JSON data writer. </summary>
    public class JSONWriter : IDisposable
    {
        // ________________ Fields  ___________________________________________________________

        Int32 indendLevel;
        Boolean prettyPrint;
        TextWriter writer;
        Stack<JSONLevel> levels;

        // ________________ Construction  ___________________________________________________________

        /// <summary> Create a new instance of <see cref="JSONWriter"/> from a file. </summary>
        /// <param name="path">The filename.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public JSONWriter( String path, Boolean prettyPrint = false ) : this( new StreamWriter( path, false ), prettyPrint ) { }
        /// <summary> Create a new instance of <see cref="JSONWriter"/> from a <see cref="Stream"/>. </summary>
        /// <param name="stream">The <see cref="Stream"/>.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public JSONWriter( Stream stream, Boolean prettyPrint = false ) : this( new StreamWriter( stream ), prettyPrint ) { }
        /// <summary> Create a new instance of <see cref="JSONWriter"/> from a <see cref="TextWriter"/>. </summary>
        /// <param name="writer">The <see cref="TextWriter"/>.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public JSONWriter( TextWriter writer, Boolean prettyPrint = false )
        {
            this.writer = writer;
            this.prettyPrint = prettyPrint;
            this.levels = new Stack<JSONLevel>();
        }

        void IDisposable.Dispose()
        {
            writer.Dispose();
        }

        // ________________ Publics  ___________________________________________________________

        /// <summary>  Begin a new node.  </summary>
        /// <param name="type">Type of the node. Can only be <see cref="JSONType.Object"/> or <see cref="JSONType.Array"/>.</param>
        /// <param name="name">Name of the node. Can be default only for root node.</param>
        public void Begin( JSONType type, String name = null )
        {
            if ( levels.Count == 0 )
            {
                levels.Push( new JSONLevel( type ) );
                indendLevel += 1;
            }
            else
            {
                WritePrefix();

                if ( levels.Peek().Type == JSONType.Object )
                {
                    WriteString( name );
                    WriteColon();
                }

                levels.Push( new JSONLevel( type ) );
                indendLevel += 1;
            }

            switch ( type )
            {
                case JSONType.Object:
                    WriteBegin( '{' );
                    break;
                case JSONType.Array:
                    WriteBegin( '[' );
                    break;
            }
        }

        /// <summary>  Write a node.  </summary>
        /// <param name="type">Typeof the node. Can be anything but Object or Array.</param>
        /// <param name="name">Name of the node.</param>
        /// <param name="value">Value of the node.</param>
        public void Write( JSONType type, String name, String value )
        {
            WritePrefix();

            if ( levels.Peek().Type == JSONType.Object )
            {
                WriteString( name );
                WriteColon();
            }

            switch ( type )
            {
                case JSONType.String:
                    WriteString( value );
                    break;
                case JSONType.Number:
                    WriteNumber( value );
                    break;
                case JSONType.Null:
                    WriteNull();
                    break;
                case JSONType.True:
                    WriteBoolean( true );
                    break;
                case JSONType.False:
                    WriteBoolean( false );
                    break;
                default:
                    break;
            }
        }

        /// <summary>  End the current node.  </summary>
        public void End()
        {
            if ( indendLevel > 0 )
                indendLevel -= 1;

            switch ( levels.Peek().Type )
            {
                case JSONType.Object:
                    WriteEnd( '}' );
                    break;
                case JSONType.Array:
                    WriteEnd( ']' );
                    break;
            }

            levels.Pop();
        }

        // ________________ Privates  ___________________________________________________________

        void WriteIndent()
        {
            if ( prettyPrint )
            {
                for ( var i = 0; i < indendLevel; i++ )
                    writer.Write( "    " );
            }
        }

        void WriteBegin( Char c )
        {
            if ( !prettyPrint )
                writer.Write( c );
            else
                writer.WriteLine( c );
        }

        void WriteEnd( Char c )
        {
            if ( !prettyPrint )
            {
                writer.Write( c );
            }
            else
            {
                writer.WriteLine();
                WriteIndent();
                writer.Write( c );
            }
        }

        void WriteComma()
        {
            if ( !prettyPrint )
                writer.Write( ',' );
            else
                writer.WriteLine( ',' );
        }

        void WriteColon()
        {
            if ( !prettyPrint )
                writer.Write( ':' );
            else
                writer.Write( " : " );
        }

        void WriteString( String str )
        {
            writer.Write( '"' );

            for ( var i = 0; i < str.Length; i++ )
                writer.Write( str[ i ] );

            writer.Write( '"' );
        }

        void WriteNumber( String str )
        {
            for ( var i = 0; i < str.Length; i++ )
                writer.Write( str[ i ] );
        }

        void WriteNull()
        {
            writer.Write( "null" );
        }

        void WriteBoolean( Boolean value )
        {
            writer.Write( value ? "true" : "false" );
        }

        void WritePrefix()
        {
            var level = levels.Peek();

            switch ( level.Type )
            {
                case JSONType.Object:
                    if ( level.NumberOfNodes > 0 )
                    {
                        WriteComma();
                    }
                    break;
                case JSONType.Array:
                    if ( level.NumberOfNodes > 0 )
                    {
                        WriteComma();
                    }
                    break;
                default:
                    break;
            }

            level.NumberOfNodes += 1;

            WriteIndent();
        }
    };
}
