﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Kodo.JSON
{
    /// <summary> Represents a JSON type. </summary>
    public enum JSONType
    {
        /// <summary> Object. </summary>
        Object,
        /// <summary> Array. </summary>
        Array,
        /// <summary> String. </summary>
        String,
        /// <summary> Number. </summary>
        Number,
        /// <summary> Null. </summary>
        Null,
        /// <summary> False. </summary>
        False,
        /// <summary> True. </summary>
        True
    }

    /// <summary> Represents a JSON element. </summary>
    public class JSONNode : IEnumerable<JSONNode>
    {
        JSONNode parent;
        JSONNode memberFirst;
        JSONNode memberLast;
        JSONNode siblingNext;
        JSONNode siblingPrev;

        JSONType type;
        String name;
        String content;

        /// <summary> Create a new instance of <see cref="JSONNode"/>. </summary>
        public JSONNode() : this( JSONType.Object, String.Empty, String.Empty ) { }
        /// <summary> Create a new instance of <see cref="JSONNode"/> with the specified type. </summary>
        /// <param name="type">The type of the node.</param>
        public JSONNode( JSONType type ) : this( type, String.Empty, String.Empty ) { }
        /// <summary> Create a new instance of <see cref="JSONNode"/> with the specified type and name. </summary>
        /// <param name="type">The type of the node.</param>
        /// <param name="name">The name of the node.</param>
        public JSONNode( JSONType type, String name ) : this( type, name, String.Empty ) { }
        /// <summary> Create a new instance of <see cref="JSONNode"/> with the specified type, name and content. </summary>
        /// <param name="type">The type of the node.</param>
        /// <param name="name">The name of the node.</param>
        /// <param name="content">The content of the node.</param>
        public JSONNode( JSONType type, String name, String content )
        {
            this.type = type;
            this.name = name;
            this.content = content;
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
        /// <summary> Returns an enumerator that enumerates through the nodes contained in this <see cref="JSONNode"/>. </summary>
        public IEnumerator<JSONNode> GetEnumerator()
        {
            for ( var node = memberFirst; node != null; node = node.siblingNext )
                yield return node;
        }

        /// <summary> Get an iterator that iterates through the nodes contained in this <see cref="JSONNode"/>. </summary>
        public IEnumerable<JSONNode> Members
        {
            // Since we implement IEnumerable<T> and the default iteration is memberwise, just return this node.
            get { return this; }
        }

        /// <summary> Get an iterator that iterates through the siblings of this <see cref="JSONNode"/>. </summary>
        public IEnumerable<JSONNode> Siblings
        {
            get
            {
                for ( var node = siblingNext; node != null; node = node.siblingNext )
                    yield return node;
            }
        }

        /// <summary> Return the <see cref="JSONNode"/> from the members of this <see cref="JSONNode"/> that has the specified name or null if not found. </summary>
        /// <param name="name">Name of the <see cref="JSONNode"/> to find.</param>
        public JSONNode GetNode( String name )
        {
            for ( var node = memberFirst; node != null; node = node.siblingNext )
            {
                if ( node.Name == name )
                    return node;
            }

            return null;
        }

        /*static JSONNode GetNode( String name, JSONNode node )
        {
            foreach ( var member in node.Members )
            {
                if ( member.name == name )
                {
                    return member;
                }
                else
                {
                    var res = GetNode( name, member );

                    if ( res != null )
                        return res;
                }
            }

            return null;
        }*/

        /// <summary> The parent <see cref="JSONNode"/> of this <see cref="JSONNode"/>. </summary>
        public JSONNode Parent
        {
            get { return parent; }
        }

        /// <summary> Get or set the node name. </summary>
        public String Name
        {
            get { return name; }
            set
            {
                if ( parent == null )
                    throw new InvalidOperationException( "Root node can not have a name." );
                if ( parent.Type != JSONType.Object )
                    throw new InvalidOperationException( "Only nodes within a object may have a name." );

                name = value;
            }
        }

        /// <summary> Get or set the value of the node. </summary>
        public String Value
        {
            get { return content; }
            set
            {
                if ( type != JSONType.String || type != JSONType.Number )
                    throw new InvalidOperationException( "Only string or number nodes may have a value." );

                content = value;
            }
        }

        /// <summary> Gets the node value as a boolean. </summary>
        Boolean AsBoolean()
        {
            Debug.Assert( type == JSONType.True || type == JSONType.False );
            return (type == JSONType.True);
        }

        void SetValue( Boolean value )
        {
            Type = (value ? JSONType.True : JSONType.False);
        }

        /*template<typename typeof_number>
        /// <summary> Gets the node value as a Number. </summary>
        typeof_number GetValueAsNumber() 
        {
            Debug.Assert( type == JsonType.Number );
            return internal.json_to_number<typeof_number>( content );
        }

        template<typename typeof_number>
        /// <summary> Gets the node value as a Number. </summary>
        void set_number( typeof_number value )
        {
            set_type( JsonType.Number );
            content.assign( std.to_wstring( value ) );
        }*/

        /// <summary> Get or set the node type. (NOTE: Setting the node type completely resets the node!) </summary>
        public JSONType Type
        {
            get { return type; }
            set
            {
                if ( type != value )
                {
                    RemoveAll();
                    content = String.Empty;
                    type = value;
                }
            }
        }

        /// <summary> Indicates whether or not the node contains the specified member. </summary>
        /// <param name="name"> Name of the member to check. </param>
        public Boolean Has( String name )
        {
            for ( var member = memberFirst; member != null; member = member.siblingNext )
            {
                if ( member.name == name )
                    return true;
            }

            return false;
        }

        /// <summary>  Gets the first member with the specified name. Returns null if not found.  </summary>
        /// <param name="name"> Name of the member to get. </param>
        public JSONNode this[ String name ]
        {
            get { return Get( name ); }
        }

        /// <summary> 
        /// Gets the member with the specified index. Returns null if not found. 
        /// </summary>
        /// <param name="index"> Index of the member to Get. </param>
        public JSONNode this[ Int32 index ]
        {
            get { return Get( index ); }
        }

        /// <summary> Gets the first member with the specified name. Returns null if not found. </summary>
        /// <param name="name"> Name of the member to Get. </param>
        public JSONNode Get( String name )
        {
            Debug.Assert( type == JSONType.Object );

            for ( JSONNode member = memberFirst; member != null; member = member.siblingNext )
            {
                if ( member.name == name )
                    return member;
            }

            return null;
        }

        /// <summary> Gets the member with the specified index. Returns null if not found. </summary>
        /// <param name="index"> Index of the member to Get. </param>
        public JSONNode Get( Int32 index )
        {
            Debug.Assert( type == JSONType.Object || type == JSONType.Array );

            Int32 member_counter = 0;

            for ( JSONNode member = memberFirst; member != null; member_counter++, member = member.siblingNext )
            {
                if ( member_counter == index )
                    return member;
            }

            return null;
        }

        /// <summary> Prepends a new member. </summary>
        public JSONNode Prepend( JSONNode value )
        {
            return InternalInsert( memberFirst, value );
        }

        /// <summary> Appends a new member. </summary>
        public JSONNode Append( JSONNode value )
        {
            return InternalInsert( null, value );
        }

        /// <summary> Inserts a new member node at specified place inside the node. </summary>
        /// <param name="at">Place where to insert the member, or to insert at the back.</param>
        /// <param name="value">Node to insert.</param>
        public JSONNode Insert( JSONNode at, JSONNode value )
        {
            return InternalInsert( at, value );
        }

        /// <summary> Removes specified member from the node </summary>
        /// <param name="name">Name of the member to be removed.</param>
        public void Remove( String name )
        {
            Remove( Get( name ) );
        }

        /// <summary> Removes specified member from the node </summary>
        /// <param name="member">Name of the member to be removed.</param>
        public void Remove( JSONNode member )
        {
            if ( member != null )
                InternalRemove( member );
        }

        /// <summary> Removes all members. </summary>
        public void RemoveAll()
        {
            for ( JSONNode member = memberFirst; member != null; member = member.siblingNext )
            {
                member.parent = null;
            }

            memberFirst = null;
        }

        JSONNode InternalInsert( JSONNode at, JSONNode member )
        {
            Debug.Assert( at == null || at.parent == this );
            Debug.Assert( member != null && member.parent == null );
            Debug.Assert( type == JSONType.Object || type == JSONType.Array );

            if ( at == memberFirst )
            {
                if ( memberFirst != null )
                {
                    member.siblingNext = memberFirst;
                    memberFirst.siblingPrev = member;
                }
                else
                {
                    member.siblingNext = null;
                    memberLast = member;
                }

                memberFirst = member;

                member.parent = this;
                member.siblingPrev = null;
            }
            else if ( at == null )
            {
                if ( memberFirst != null )
                {
                    member.siblingPrev = memberLast;
                    memberLast.siblingNext = member;
                }
                else
                {
                    member.siblingPrev = null;
                    memberFirst = member;
                }

                memberLast = member;

                member.parent = this;
                member.siblingNext = null;
            }
            else
            {
                member.siblingPrev = at.siblingPrev;
                member.siblingNext = at;
                at.siblingPrev.siblingNext = member;
                at.siblingPrev = member;
                member.parent = this;
            }

            return member;
        }

        void InternalRemove( JSONNode member )
        {
            Debug.Assert( member != null && member.parent == this );
            Debug.Assert( memberFirst != null );

            if ( member == memberFirst )
            {
                JSONNode temp = memberFirst;
                memberFirst = temp.siblingNext;

                if ( temp.siblingNext != null )
                    temp.siblingNext.siblingPrev = null;
                else
                    memberLast = null;

                temp.parent = null;
            }
            else if ( member == memberLast )
            {
                JSONNode temp = memberLast;

                if ( temp.siblingPrev != null )
                {
                    memberLast = temp.siblingPrev;
                    temp.siblingPrev.siblingNext = null;
                }
                else
                    memberFirst = null;

                temp.parent = null;
            }
            else
            {
                member.siblingPrev.siblingNext = member.siblingNext;
                member.siblingNext.siblingPrev = member.siblingPrev;
                member.parent = null;
            }
        }
    };
}
