﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

//
// Assembly information.
//
[assembly: ComVisible( false )]
[assembly: Guid( "e0a6bf0a-14ae-4bd7-8679-0d633b43f1e6" )]
[assembly: AssemblyTitle( "kodo-json" )]
[assembly: AssemblyDescription( "" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "" )]
[assembly: AssemblyProduct( "kodo-json" )]
[assembly: AssemblyCopyright( "Copyright © Joonas Anttonen 2015" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

namespace Kodo.JSON
{
    /// <summary> JSON document convenience class. </summary>
    public class JSONDocument : JSONNode
    {
        // ________________ Construction  ___________________________________________________________

        /// <summary> Create a new instance of <see cref="JSONDocument"/>. </summary>
        public JSONDocument() { }
        /// <summary> Create a new instance of <see cref="JSONDocument"/> from a string. </summary>
        /// <param name="input">The string to read from.</param>
        public JSONDocument( String input ) { Read( input ); }
        /// <summary> Create a new instance of <see cref="JSONDocument"/> from a stream. </summary>
        /// <param name="input">The stream to read from.</param>
        public JSONDocument( Stream input ) { Read( input ); }

        // ________________ Publics  ___________________________________________________________

        /// <summary> Read the JSON document from the specified <see cref="Stream"/>. </summary>
        /// <param name="input">The <see cref="Stream"/> to read from.</param>
        public void Read( Stream input )
        {
            using ( var reader = new StreamReader( input ) )
            {
                Read( reader );
            }
        }

        /// <summary> Read the JSON document from the specified <see cref="String"/>. </summary>
        /// <param name="input">The <see cref="String"/> to read from.</param>
        public void Read( String input )
        {
            using ( var reader = new StringReader( input ) )
            {
                Read( reader );
            }
        }

        /// <summary> Read the JSON document from the specified <see cref="TextReader"/>. </summary>
        /// <param name="input">The <see cref="TextReader"/> to read from.</param>
        public void Read( TextReader input )
        {
            using ( var reader = new JSONReader( input ) )
            {
                Read( reader );
            }
        }

        /// <summary> Write the JSON data to the specified file. </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="append">Whether or not to append the data to the file.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public void Write( String filename, Boolean append, Boolean prettyPrint = false )
        {
            using ( var writer = new StreamWriter( filename, append ) )
            {
                Write( writer, prettyPrint );
            }
        }

        /// <summary> Write the JSON data to the specified <see cref="Stream"/>. </summary>
        /// <param name="output">The <see cref="Stream"/>.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public void Write( Stream output, Boolean prettyPrint = false )
        {
            using ( var writer = new StreamWriter( output ) )
            {
                Write( writer, prettyPrint );
            }
        }

        /// <summary> Write the JSON data to the specified <see cref="TextWriter"/>. </summary>
        /// <param name="output">The <see cref="TextWriter"/>.</param>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public void Write( TextWriter output, Boolean prettyPrint = false )
        {
            using ( var writer = new JSONWriter( output, prettyPrint ) )
            {
                Write( writer, this );
            }
        }

        /// <summary> Convert the JSON data to a <see cref="String"/>. </summary>
        public override String ToString() { return ToString( false ); }
        /// <summary> Convert the JSON data to a <see cref="String"/>. </summary>
        /// <param name="prettyPrint">Whether or not to pretty print the data.</param>
        public String ToString( Boolean prettyPrint )
        {
            var builder = new StringBuilder();

            using ( var writer = new StringWriter( builder ) )
            {
                Write( writer, prettyPrint );
            }

            return builder.ToString();
        }

        // ________________ Privates  ___________________________________________________________

        void Read( JSONReader reader )
        {
            JSONNode currentNode = this;

            while ( true )
            {
                switch ( reader.Read() )
                {
                    case JSONReaderResult.BeginDocument:
                        currentNode.Type = reader.Type;
                        break;
                    case JSONReaderResult.BeginElement:
                        currentNode = currentNode.Append( new JSONNode( reader.Type, reader.Name, reader.Value ) );
                        break;
                    case JSONReaderResult.Node:
                        currentNode.Append( new JSONNode( reader.Type, reader.Name, reader.Value ) );
                        break;
                    case JSONReaderResult.EndElement:
                        currentNode = currentNode.Parent;
                        break;
                    case JSONReaderResult.EndDocument:
                        return;
                }
            }
        }

        void Write( JSONWriter writer, JSONNode node )
        {
            switch ( node.Type )
            {
                case JSONType.Object:
                case JSONType.Array:
                    writer.Begin( node.Type, node.Name );

                    foreach ( var member in node.Members )
                        Write( writer, member );

                    writer.End();
                    break;
                default:
                    writer.Write( node.Type, node.Name, node.Value );
                    break;
            }
        }
    }
}
