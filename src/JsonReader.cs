﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Kodo.JSON
{
    internal class JSONLevel
    {
        public JSONType Type { get; set; }
        public Int32 NumberOfNodes { get; set; }

        public JSONLevel( JSONType type )
        {
            this.Type = type;
            this.NumberOfNodes = 0;
        }
    }

    /// <summary> JSON exception. </summary>
    public class JSONException : Exception
    {
        /// <summary> Create a new instance of <see cref="JSONException"/>. </summary>
        /// <param name="message">The message.</param>
        public JSONException( String message ) : base( message ) { }
    }

    /// <summary> Represents <see cref="JSONReader"/> result. </summary>
    public enum JSONReaderResult
    {
        /// <summary> The beginning of the document. </summary>
        BeginDocument,
        /// <summary> The beginning of a collection of nodes. </summary>
        BeginElement,
        /// <summary> Node. </summary>
        Node,
        /// <summary> The end of the document. </summary>
        EndDocument,
        /// <summary> The end of a collection of nodes. </summary>
        EndElement
    }

    /// <summary> JSON data reader. </summary>
    public class JSONReader : IDisposable
    {
        Stack<JSONLevel> levels = new Stack<JSONLevel>();

        TextReader reader;
        JSONReaderResult result;

        JSONType currentType;
        String currentName;
        String currentContent;

        /// <summary> Create a new instance of <see cref="JSONReader"/> from a file. </summary>
        /// <param name="path">The filename.</param>
        public JSONReader( String path ) : this( new StreamReader( path, Encoding.UTF8 ) ) { }
        /// <summary> Create a new instance of <see cref="JSONReader"/> from a <see cref="Stream"/>. </summary>
        /// <param name="stream">The <see cref="Stream"/>.</param>
        public JSONReader( Stream stream ) : this( new StreamReader( stream, Encoding.UTF8 ) ) { }
        /// <summary> Create a new instance of <see cref="JSONReader"/> from a <see cref="TextReader"/>. </summary>
        /// <param name="reader">The <see cref="TextReader"/>.</param>
        public JSONReader( TextReader reader )
        {
            this.reader = reader;
        }

        void IDisposable.Dispose()
        {
            reader.Dispose();
        }

        /// <summary>  Get the current node type.  </summary>
        public JSONType Type
        {
            get { return currentType; }
        }

        /// <summary>  Get the current node name.  </summary>
        public String Name
        {
            get { return currentName; }
        }

        /// <summary> Get the current node value. </summary>
        public String Value
        {
            get { return currentContent; }
        }

        /// <summary> 
        /// Read the Next node from the stream. 
        /// </summary>
        public JSONReaderResult Read()
        {
            // Reset current node data.
            currentName = String.Empty;
            currentContent = String.Empty;

            if ( levels.Count == 0 )
            {
                if ( result == JSONReaderResult.EndElement )
                {
                    // This is the end of the document.
                    result = JSONReaderResult.EndDocument;
                }
                else
                {
                    // This is the beginning of the document.
                    result = JSONReaderResult.BeginDocument;

                    ReadWhitespace();

                    switch ( reader.Peek() )
                    {
                        case '{':
                            // Root is an object.
                            ReadBeginElement( JSONType.Object );
                            break;
                        case '[':
                            // Root is an .Array.
                            ReadBeginElement( JSONType.Array );
                            break;
                        default:
                            throw new JSONException( "Must be either an object or an array at root." );
                    }
                }
            }
            else
            {
                // Continue according to current type.
                switch ( levels.Peek().Type )
                {
                    case JSONType.Object:
                        result = ReadObject();
                        break;
                    case JSONType.Array:
                        result = ReadArray();
                        break;
                    default:
                        throw new JSONException( "Unexpected node type." );
                }
            }

            return result;
        }

        void ReadWhitespace()
        {
            int c;

            while ( true )
            {
                c = reader.Peek();

                if ( c == ' ' || c == '\n' || c == '\r' || c == '\t' )
                    reader.Read();
                else
                    break;
            }
        }

        JSONReaderResult ReadValue()
        {
            ReadWhitespace();

            levels.Peek().NumberOfNodes += 1;

            switch ( reader.Peek() )
            {
                case '{':
                    return ReadObject();
                case '[':
                    return ReadArray();
                case 'n':
                    currentType = JSONType.Null;
                    ReadNull();
                    break;
                case 't':
                    currentType = JSONType.True;
                    ReadTrue();
                    break;
                case 'f':
                    currentType = JSONType.False;
                    ReadFalse();
                    break;
                case '"':
                    currentType = JSONType.String;
                    currentContent = ReadString();
                    break;
                default:
                    currentType = JSONType.Number;
                    currentContent = ReadNumber();
                    break;
            }

            return JSONReaderResult.Node;
        }

        JSONReaderResult ReadBeginElement( JSONType type )
        {
            reader.Read();
            levels.Push( new JSONLevel( type ) );
            currentType = type;
            return JSONReaderResult.BeginElement;
        }

        JSONReaderResult ReadEndElement()
        {
            reader.Read();
            levels.Pop();
            return JSONReaderResult.EndElement;
        }

        JSONReaderResult ReadObject()
        {
            ReadWhitespace();

            if ( reader.Peek() == '{' )
                return ReadBeginElement( JSONType.Object );

            if ( reader.Peek() == '}' )
                return ReadEndElement();

            if ( levels.Peek().NumberOfNodes > 0 )
            {
                if ( reader.Read() != ',' )
                    throw new JSONException( "Object members must be separated with ','." );

                ReadWhitespace();
            }

            if ( reader.Peek() != '"' )
                throw new JSONException( "Name of an object member must be a String." );

            currentName = ReadString();
            ReadWhitespace();

            if ( reader.Read() != ':' )
                throw new JSONException( "Object member names must be followed by ':'." );

            return ReadValue();
        }

        JSONReaderResult ReadArray()
        {
            ReadWhitespace();

            if ( reader.Peek() == '[' )
                return ReadBeginElement( JSONType.Array );

            if ( reader.Peek() == ']' )
                return ReadEndElement();

            if ( levels.Peek().NumberOfNodes > 0 )
            {
                if ( reader.Read() != ',' )
                    throw new JSONException( "Elements must be delimeted with ','." );
            }

            return ReadValue();
        }

        static readonly Byte[] EscapeChars = new Byte[ 256 ]
        {
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(Byte)'\"',0,0,0,0,0,0,0,0,0,0,0,0,(Byte)'/', 
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(Byte)'\\',0,0,0, 
			0,0,(Byte)'\b',0,0,0,(Byte)'\f',0,0,0,0,0,0,0,(Byte)'\n',0, 0,0,(Byte)'\r',0,(Byte)'\t',0,0,0,0,0,0,0,0,0,0,0, 
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		};

        String ReadString()
        {
            Debug.Assert( reader.Peek() == '"' );

            reader.Read();

            var builder = new StringBuilder();

            while ( true )
            {
                var c = reader.Peek();

                if ( c == '\\' )
                {
                    reader.Read();
                    c = reader.Read();

                    if ( c < 256 && (EscapeChars[ (Byte)c ] != 0) )
                    {
                        builder.Append( (Char)EscapeChars[ (Byte)c ] );
                    }
                    else if ( c == 'u' )
                    {
                        UInt32 codepoint = 0;
                        for ( var i = 0; i < 4; i++ )
                        {
                            var cc = (Char)reader.Read();

                            codepoint <<= 4;
                            codepoint += cc;

                            if ( cc >= '0' && cc <= '9' )
                                codepoint -= '0';
                            else if ( cc >= 'A' && cc <= 'F' )
                                codepoint -= 'A' - 10;
                            else if ( cc >= 'a' && cc <= 'f' )
                                codepoint -= 'a' - 10;
                            else
                                throw new JSONException( "Incorrect hex digit after \\u escape." );
                        }

                        builder.Append( (Char)codepoint );
                    }
                    else
                    {
                        throw new JSONException( "Unknown escape character." );
                    }

                }
                else if ( c == '"' )
                {
                    reader.Read();
                    break;
                }
                else
                {
                    builder.Append( (Char)reader.Read() );
                }
            }

            return builder.ToString();
        }

        String ReadNumber()
        {
            var builder = new StringBuilder();

            if ( reader.Peek() == '-' )
                builder.Append( (Char)reader.Read() );

            if ( reader.Peek() == '0' )
                builder.Append( (Char)reader.Read() );
            else if ( reader.Peek() >= '1' && reader.Peek() <= '9' )
            {
                builder.Append( (Char)reader.Read() );

                while ( reader.Peek() >= '0' && reader.Peek() <= '9' )
                    builder.Append( (Char)reader.Read() );
            }
            else
                throw new JSONException( "Expected a number here." );

            if ( reader.Peek() == '.' )
            {
                builder.Append( (Char)reader.Read() );

                if ( reader.Peek() >= '0' && reader.Peek() <= '9' )
                    builder.Append( (Char)reader.Read() );
                else
                    throw new JSONException( "Need atleast one digit in fraction part." );

                while ( reader.Peek() >= '0' && reader.Peek() <= '9' )
                    builder.Append( (Char)reader.Read() );
            }

            if ( reader.Peek() == 'e' || reader.Peek() == 'E' )
            {
                builder.Append( (Char)reader.Read() );

                if ( reader.Peek() == '+' )
                    builder.Append( (Char)reader.Read() );
                else if ( reader.Peek() == '-' )
                    builder.Append( (Char)reader.Read() );

                if ( reader.Peek() >= '0' && reader.Peek() <= '9' )
                {
                    builder.Append( (Char)reader.Read() );

                    while ( reader.Peek() >= '0' && reader.Peek() <= '9' )
                        builder.Append( (Char)reader.Read() );
                }
                else
                    throw new JSONException( "Need atleast one digit in exponent part." );
            }

            return builder.ToString();
        }

        void ReadFalse()
        {
            Debug.Assert( reader.Peek() == 'f' );

            reader.Read();

            if ( reader.Read() != 'a' || reader.Read() != 'l' || reader.Read() != 's' || reader.Read() != 'e' )
                throw new JSONException( "Invalid value." );
        }

        void ReadTrue()
        {
            Debug.Assert( reader.Peek() == 't' );

            reader.Read();

            if ( reader.Read() != 'r' || reader.Read() != 'u' || reader.Read() != 'e' )
                throw new JSONException( "Invalid value." );
        }

        void ReadNull()
        {
            Debug.Assert( reader.Peek() == 'n' );

            reader.Read();

            if ( reader.Read() != 'u' || reader.Read() != 'l' || reader.Read() != 'l' )
                throw new JSONException( "Invalid value." );
        }
    }
}
